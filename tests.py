import unittest
from game_of_life import Cell,rungame

class test_game_of_life(unittest.TestCase):
    def setUp(self):
        print('setting up')
    def tearDown(setUp):
        print()
    def test_neighbours(self):
        testcell = Cell(True,[0,0],5)
        for items in testcell.findNeighbours():
            self.assertTrue(items in [[0,1],[1,1],[1,0]])
        self.assertEqual(len(testcell.findNeighbours()),3)
    def test_game(self):
        testgame = rungame(2,[[0,0]])
        # testing rungame initialised the grid correctly
        for cell in [(0,0)]:
            self.assertTrue(testgame.initialstatus[cell].alive)
        for cell in [(0,1),(1,0),(1,1)]:
            self.assertFalse(testgame.initialstatus[cell].alive)
        # testing next_generation_status
        for cell in [(0,0),(0,1),(1,0),(1,1)]:
            self.assertFalse(testgame.next_generation_status(cell))
        # tesing update_grid
        testgame.update_grid()
        for cell in [(0,0),(0,1),(1,0),(1,1)]:
            self.assertFalse(testgame.initialstatus[cell].alive)
        testgame.display_grid()
if __name__ == '__main__':
    unittest.main()
