class Cell(object):

    def __init__(self, alive, position, grid_size):
        self.alive = alive
        self.position = position
        self.grid_size = grid_size

    def findNeighbours(self):
        x = self.position[0]
        y = self.position[1]
        neighbours = [[x-1, y+1], [x, y+1], [x+1, y+1], [x+1, y], [x+1, y-1], [x, y-1], [x-1, y-1], [x-1, y]]
        get_rid_of = []
        for neighbour in neighbours:
            if any([neighbour[0] < 0, neighbour[0] >= self.grid_size, neighbour[1] < 0, neighbour[1] >= self.grid_size]):
                get_rid_of.append(neighbour)
        for bad_neighbour in get_rid_of:
            # if bad_neighbour in neighbours:
            neighbours.remove(bad_neighbour)
        return neighbours

class rungame(Cell):

    def __init__(self,grid_size,living_cells):
        self.grid_size = grid_size
        self.initial_number_of_living_cells = len(living_cells)
        # Cell.__init__(alive, position, grid_size)
        self.initialstatus = {}
        for i in range(grid_size):
            for j in range(grid_size):
                self.initialstatus[(i,j)] = Cell(False,[i,j],grid_size)
        for cell in living_cells:
            self.initialstatus[tuple(cell)] = Cell(True,cell, grid_size)
        # print(self.initialstatus)

    def next_generation_status(self, position):
        """We are feeding in position as a tuple here"""
        neighbours = self.initialstatus[position].findNeighbours()
        number_of_living_neighbours = 0
        for neighbour in neighbours:
            if self.initialstatus[tuple(neighbour)].alive:
                number_of_living_neighbours += 1

        if self.initialstatus[position].alive:
            if number_of_living_neighbours in [2, 3]:
                return True
            else:
                return False
        else:
            if number_of_living_neighbours == 3:
                return True
            else:
                return False

    def update_grid(self):
        for cell in self.initialstatus.keys():
            self.initialstatus[cell].alive = self.next_generation_status(cell)

    def display_grid(self):
        for x in range(self.grid_size):
            for y in range(self.grid_size):
                if y == (self.grid_size - 1):
                    if self.initialstatus[(x,y)].alive:
                        print(1)
                    else:
                        print(0)
                else:
                    if self.initialstatus[(x,y)].alive:
                        print(1, end = '')
                    else:
                        print(0, end = '')


    def play_game(self):
        number_of_living_cells = self.initial_number_of_living_cells
        while number_of_living_cells != 0:
            # self.display_grid()
            self.update_grid()
            number_of_living_cells = 0
            for cell in self.initialstatus.keys():
                if self.initialstatus[cell].alive:
                    number_of_living_cells += 1


    # for x in range(grid_size)
    #     for y in range
    #         if self.inital:
    #             print 1, end=''
    #     print('\n')''

# if __name__ == '__main__':
    # testcell = Cell(True,[0,1],5)
    # print(testcell.findNeighbours())
